# MNN 32 neurons
Este proyecto se ha creado para simular la red MNN de 32 neuronas que deseamos enviar a fabricar en chip.

Este diseño tiene como fin operar con la base de datos del `MNIST`. 

Dentro del repo estan todos los ficheros fuentes en `.vhd` salvo el testbench que está escrito en `verilog` : `tb_maxmin_top_verilog.v`. 

En la subcarpeta `vcd` he puesto la última simulación que realicé usando el `ModelSim` para comprobar con el `vcd` generado por la herramienta de sintesis de `CADENCE`. 

A partir de la version `v1.2` se utiliza un workbench que lee una imagen desde un fichero csv que se encuentra en : [`input_image/x_test_img1.csv`](input_image/x_test_img1.csv). Este fichero debe de estar ubicado en el `work` de la herramienta de sintesis para que lo pueda encontrar. 

Además de esto, se ha agregado el comando para la herramienta `XCELIUM` para simular el RTL en `CADENCE` (ver [XCELIUM_command.txt](src/XCELIUM_command.txt)).



# Métricas
- Accuracy Software: `95.04%`
- Accuracy en Hardware (using SC): `93.21%`
- Numero de pesos: 1994
> Para ver una tabla de comparaciones con otros trabajos relacionados:
https://docs.google.com/spreadsheets/d/1xr4n5CZm85Ck4icC0S9bVhje0SyLkYXcXJuC2eO3XFc/edit#gid=1231130919.


# Diagrama de diseño top : `maxmin_top.vhd`


![Diagram](_Doc/maxmin_top.svg "Diagram")
## Ports

| Port name | Direction | Type                         | Description |
| --------- | --------- | ---------------------------- | ----------- |
| clk       | in        | std_logic                    |             |
| rst       | in        | std_logic                    | Reset all, active high            |
| i_en      | in        | std_logic                    | Enable processing, active high            |
| i_x_data  | in        | std_logic_vector(7 downto 0) | Input Data            |
| i_x_wren  | in        | std_logic                    | Write enable for the input sift register            |
| i_y_add   | in        | std_logic_vector(3 downto 0) | Address for the output MUX            |
| o_result  | out       | std_logic_vector(7 downto 0) | Output of the MUX            |
| o_done    | out       | std_logic                    | when the processing is finished, this signal goes '1'            |

# Diagrama en bloques de `maxmin_net.vhd`

![Alt text](_Doc/maxmin_net_blocks.png)

# VLSI RESULTS

![](_Doc/CHIP.png)

![](_Doc/chip_simulation_post_synthesis.png)

# HISTORICO

Para ver más detalles del historial, ver los comentarios de los `commits` realizados.

```
------------------------------
Fecha       Version     Detalles
------------------------------
01/07/2023  1.0         Versión inicial
20/09/2023  1.2         - Se ha disminuido el numero de pines 
                          necesarios a un total de 25 (sin contar 
                          alimentaciones) cambiando la forma de 
                          escribir las entradas. Ahora se usa un
                          "shift register", de manera que la 
                          entrada de direcciones que antes 
                          necesitabamos para escribir las entradas 
                          ya no son necesarias.
                        - Indeterminaciones de algunas señales se han quitado
                        - Testbench leyendo una imagen desde un fichero csv: "x_test_img1.csv".
06/10/2023  1.3         - generación de csv para las salidas

```

