-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 29.10.2021 10:19:46 UTC

library ieee;
use STD.textio.all;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.maxmin_net_pkg.all;

entity tb_maxmin_net is
end tb_maxmin_net;

architecture tb of tb_maxmin_net is

    component maxmin_net is
        Port (
            clock, reset, enable: IN STD_LOGIC;
            lfsr1_seed, lfsr2_seed : IN STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0);
            i_x : in pkg_xi_array_t;
            i_w : in pkg_w_array_t;
            o_y : out pkg_y_slv_array_t;		
            o_done : out std_logic;
            o_next : out std_logic
        );
    end component;

    component weights_hardwired IS	
    PORT
    (
        --Outputs
        weights : out pkg_w_array_t   
    );    
    END component;

    signal clock       : std_logic;
    signal reset       : std_logic;
    signal enable      : std_logic;
    signal lfsr1_seed  : std_logic_vector (pkg_bit_precission-1 downto 0);
    signal lfsr2_seed  : std_logic_vector (pkg_bit_precission-1 downto 0);
    signal i_x     : pkg_xi_array_t;
    signal i_w     : pkg_w_array_t;
    signal o_y    : pkg_y_slv_array_t;
    signal o_done : std_logic;
    signal o_next : std_logic;



    constant TbPeriod : time := 20 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

    file file_csv : text;

begin

    lfsr1_seed <= std_logic_vector(to_unsigned(255, lfsr1_seed'length));
    lfsr2_seed <= std_logic_vector(to_unsigned(97,  lfsr2_seed'length));

    dut : maxmin_net
    port map (clock       => clock,
              reset       => reset,
              enable      => enable,
              lfsr1_seed  => lfsr1_seed,
              lfsr2_seed  => lfsr2_seed,
              i_x     => i_x,
              i_w     => i_w,
              o_y    => o_y,
              o_done => o_done,
              o_next => o_next);

    -- Instantation 
    weights_inst: weights_hardwired
    port map(i_w);


    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clock is really your main clock signal
    clock <= TbClock;

    stimuli : process
        variable v_ILINE: line;
        variable i : integer;
        variable v_tmpInt: integer;
        variable tmpchar : character;
    begin
        -- EDIT Adapt initialization as needed
        reset <= '1';
        enable <= '0';

        -- csv reading
        file_open(file_csv, "x_test_img1.csv", read_mode);
        i := 0;        
        while not endfile(file_csv) loop
            ----------------------------------
            --Read the input
            ----------------------------------
            readline(file_csv, v_ILINE);            
            
            read(v_ILINE, v_tmpInt);
            i_x(i) <= to_signed(v_tmpInt, pkg_bit_precission);
            i := i + 1;
            
        end loop;
        file_close(file_csv);
    

        wait for 2 * TbPeriod;

        reset <= '0';
        enable <= '1';

        
        wait until o_done = '1';
        wait for 2 * TbPeriod;


        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';

        wait;
    end process;

end tb;
