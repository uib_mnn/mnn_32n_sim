-- autogenerate, apc v25
 --============================================================
-- Design Unit: 
-- Purpose: 
-- 
-- Authors: 
-- WEB info: 
---------------------------------------------------------------
-- [INFO]

---------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 1 			CCFF            11/12/2018              Initial
-- 2                                                    apc bipolar
-- 2.1                                                  suma y resta 1's
-- 2.2                                                  unsigned acc y convierte despues
-- 2.3                          13/09/2019              separacion por bloques
-- 2.4			CCFF			10/10/2019			    neurona con apc_scale variable
-- 2.5          CCFF            30/3/2020               Optimizacion de speed seleccionable
--=============================================================
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use work.MyTypes.all;
USE work.maxmin_net_pkg.all;

ENTITY apc IS 
    GENERIC(
        N_INPUTS : INTEGER := 4;
        INTG_PRD_CLK : INTEGER := 16;
        N_OUTPUTS :  INTEGER := 6 ;
        APC_MAX  :  INTEGER := 12306 ;
        APC_SCALE_WIDTH : INTEGER := 8
          
    );
    PORT(
        -- inputs
        clk: in  std_logic;
        i_get_now_strobe: in  std_logic;

        i_input: in std_logic_vector(N_INPUTS-1 downto 0); 
        -- apc scale
         
        i_apc_scale_idx : in std_logic_vector(APC_SCALE_WIDTH-1 downto 0);
        -- outputs
        o_apc_out: out signed (log2ceil(N_INPUTS+1) + log2ceil(INTG_PRD_CLK) downto 0);
        o_apc_out_scaled: out signed (N_OUTPUTS-1 downto 0)						
        );
END apc;

ARCHITECTURE Behavioral OF apc IS
    constant LOG2_INTG_PRD_CLK : integer := log2ceil(INTG_PRD_CLK); 
    constant PC_WIDTH : integer := log2ceil(N_INPUTS+1);
    constant APC_WIDTH : integer := PC_WIDTH + LOG2_INTG_PRD_CLK; 
    
    constant KDIV_APC : INTEGER := 8;
    constant LOG2_KDIV_APC : INTEGER := log2ceil(KDIV_APC);
   
    SIGNAL pc : unsigned (PC_WIDTH -1 DOWNTO 0);
    
    signal num_ones, apc_bip, num_zeros : signed (APC_WIDTH + 1 -1 downto 0);
    signal addnacc_in : unsigned (APC_WIDTH -1 downto 0);
    signal addnacc_next, addnacc_reg : unsigned (APC_WIDTH -1 downto 0);    
    signal apc_out_reg : signed (N_OUTPUTS-1 downto 0);


    signal limit_low_apc, limit_high_apc : signed (APC_WIDTH + 1 -1 downto 0);
    signal apc_scaled_down : signed (N_OUTPUTS-1 downto 0);

    -- se�ales registradas
    signal i_input_reg : std_logic_vector(N_INPUTS-1 downto 0); 
    signal pc_reg : unsigned (PC_WIDTH -1 DOWNTO 0);
    signal i_get_now_strobe_reg1, i_get_now_strobe_reg2, i_get_now_strobe_reg3 : std_logic;
    signal apc_bip_reg : signed (APC_WIDTH + 1 -1 downto 0);
    signal i_get_now_strobe_reg : std_logic;
    signal i_apc_scale_idx_reg : std_logic_vector(APC_SCALE_WIDTH-1 downto 0);

    component pcounter IS 
    GENERIC(
        N_INPUTS : INTEGER := 4        
    );
    PORT(
        -- inputs
        stoch_in: in std_logic_vector(N_INPUTS-1 downto 0); 
        -- outputs
        pc: out unsigned (log2ceil(N_INPUTS+1) -1 DOWNTO 0)
        );
    END component;

    component add_accum IS 
    GENERIC(
      INPUT_WIDTH : INTEGER := 4;
		APC_WIDTH : INTEGER := 4		
    );
    PORT(
        -- inputs
		  clk : in std_logic;
		  reset : in std_logic;
		  
        input 		: in unsigned(INPUT_WIDTH-1 downto 0); 
		  
        -- outputs
        output		: out unsigned(APC_WIDTH-1 downto 0)
    );
    END component;

    component unip2bip IS 
    GENERIC(
      INPUT_WIDTH : INTEGER := 4;
		APC_MAX : INTEGER := 1000
    );
    PORT(
        -- inputs
        unip 		: in unsigned(INPUT_WIDTH-1 downto 0); 
        -- outputs
        bip			: out signed(INPUT_WIDTH downto 0)
    );
    END component;

BEGIN



-------------------------------------------
    -- Si optimizacion en speed On:
    -- agrega DFF en path
    -------------------------------------------
    speed_opt_on_gen : if PKG_SPEED_OPT generate        
        process(clk)
        begin
            if rising_edge(clk) then
                i_input_reg <= i_input;            
            end if;
        end process;

        process(clk)
        begin
            if rising_edge(clk) then
                pc_reg <= pc;    
                i_get_now_strobe_reg1 <= i_get_now_strobe;   
                i_get_now_strobe_reg2 <= i_get_now_strobe_reg1;   
            end if;
        end process;

        process(clk)
        begin
            if rising_edge(clk) then
                apc_bip_reg <= apc_bip;
                i_get_now_strobe_reg3 <= i_get_now_strobe_reg2;    
                i_apc_scale_idx_reg <= i_apc_scale_idx;    
            end if;
        end process;
    end generate speed_opt_on_gen;
-------------------------------------------
    -- Si optimizacion en speed Off:
    -- no agrega DFF en path
    -------------------------------------------
    speed_opt_off_gen : if PKG_SPEED_OPT=False generate        
        i_input_reg <= i_input;            
        pc_reg <= pc;    
        i_get_now_strobe_reg1 <= i_get_now_strobe;   
        i_get_now_strobe_reg2 <= i_get_now_strobe_reg1;   
        apc_bip_reg <= apc_bip;
        i_get_now_strobe_reg3 <= i_get_now_strobe_reg2;    
        i_apc_scale_idx_reg <= i_apc_scale_idx;               
    end generate speed_opt_off_gen;



	-- parallel counter
	pcounter_inst : pcounter 
    generic map(N_INPUTS => N_INPUTS)
    port map(i_input_reg, pc);
    -- port map(i_input, pc);   

	-- adder accumelator
	add_accum_inst : add_accum
	generic map(INPUT_WIDTH => PC_WIDTH, APC_WIDTH => APC_WIDTH)
    port map(clk, i_get_now_strobe_reg2, pc_reg, addnacc_reg);
	-- port map(clk, i_get_now_strobe, pc, addnacc_reg);

    -- bipolar conversion	 
	unip_bip_inst : unip2bip 
	generic map(INPUT_WIDTH => APC_WIDTH, APC_MAX => APC_MAX)
	port map(addnacc_reg, apc_bip);

	
	o_apc_out_scaled <= apc_out_reg;
    o_apc_out <= apc_bip;

END Behavioral;


