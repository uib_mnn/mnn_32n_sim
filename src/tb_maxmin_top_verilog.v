
`timescale 1 ns/10 ps  // time-unit = 1 ns, precision = 10 ps

module tb_maxmin_top_verilog();
    
    localparam T = 20; 
    reg clk;
    reg rst;
    reg i_en;
    reg [7:0] i_x_data;
    reg i_x_wren;
    reg [3:0] i_y_add;
    wire [7:0] o_result;
    wire o_done;

    integer i;
    integer file;
    integer file_out;
    integer r;

    maxmin_top dut (
     .clk (clk),
     .rst (rst),
     .i_en (i_en),
     .i_x_data (i_x_data),
     .i_x_wren (i_x_wren),
     .i_y_add(i_y_add),
     .o_result (o_result),
     .o_done (o_done)
    );

    
    // clock generation
    initial begin
        clk = 1'b0;
        forever #(T/2) clk = ~clk;
    end

    // signal stimulus
    initial begin
        file=      $fopen("input_img/x_test_img.csv","r");
        file_out=  $fopen("output_csv/x_test_img_output.csv","w");

        // default state
        i_en = 1'b0;
        rst = 1'b1;
        i_x_data = 8'h00;
        i_x_wren = 1'b0;
        i_y_add  = 4'b0;
        #(1*T);
        rst = 1'b0;  

        while(!$feof(file)) begin

            // do a reset for the device before starting to operate
            i_en = 1'b0;
            rst = 1'b1;
            i_x_data = 8'h00;
            i_x_wren = 1'b0;
            i_y_add  = 4'b0;
            #(2*T);
            rst = 1'b0;  
            

            // read img
            for (i=0; i<784; i=i+1) begin
                r = $fscanf(file,"%d", i_x_data);
                if (r < 1) 
                    $finish;  

                #(1*T);
                i_x_wren = 1'b1;
                #(4*T); // 4 periods to emulate a slower master
                i_x_wren = 1'b0;
            end  

            // wait a falling edge in clk            
            while (clk == 0) begin
                #(T/2);
            end
            // do a reset for the device before starting to operate
            rst = 1'b1;
            #(2*T);
            rst = 1'b0;

            // enable de operation and wait until is finished
            i_en = 1'b1;
            wait (o_done == 1);

            // reads the output and write into the output.csv
            #(1*T);
            $fwrite(file_out,"%d,", o_result);
            #(1*T);

            for (i=0; i<8; i=i+1) begin
                i_y_add <= i_y_add + 1;
                #(1*T);
                $fwrite(file_out,"%d,", o_result);
                #(1*T);
            end

            i_y_add <= i_y_add + 1;
            #(1*T);
            $fwrite(file_out,"%d\n", o_result);
            #(1*T);
        end

        $fclose(file_out);
        $fclose(file);  

        $finish;  

    end

endmodule
