set pack_assert_off { std_logic_arith numeric_std }
# dumptcf -output tb/dump.tcf -dumpwireasnet -overwrite
# call vcdfile testpatterns.vcd -t ns
# database -open -vcd vcddb -default -timescale ns
# probe -create -all -depth all
# probe -create -all -variables
run
dumptcf -end

exit
