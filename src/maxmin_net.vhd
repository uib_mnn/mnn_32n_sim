-- autogenerate, mlp_sc_net v2

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use work.maxmin_net_pkg.all;
USE work.MyTypes.all;

-- v0

entity maxmin_net is
    Port (
        clock, reset, enable: IN STD_LOGIC;
        lfsr1_seed, lfsr2_seed : IN STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0);
        i_x : in pkg_xi_array_t;
        i_w : in pkg_w_array_t;
        o_y : out pkg_y_slv_array_t;		
        o_done : out std_logic;
        o_next : out std_logic
    );
end maxmin_net;

architecture Behavioral of maxmin_net is

    component neurona_maxmin IS
    generic (				
        GNRC_N_INPUTS                   : integer   :=4; 
        GNRC_MAXMIN_TYPE                : INTEGER   := 0;        
        GNRC_ADDITION_SCALE_DOWN        : INTEGER   := 2
    );
    PORT
    (
        -- Inputs
        clock : IN STD_LOGIC; 
        reset : IN STD_LOGIC;		
        
        i_lfsr : 		IN signed (PKG_BIT_PRECISSION-1 DOWNTO 0);
        i_x : 			IN pkg_xi_neuron_maxmin_array_t(0 to GNRC_N_INPUTS-1);
        i_w : 			IN pkg_xi_neuron_maxmin_array_t(0 to GNRC_N_INPUTS-1);
    
        --Outputs
        o_y_sc : 		OUT STD_LOGIC		
    );
    
    END component;

    ----------------------------------------
    -- MAX MIN signals
    ----------------------------------------
    -- salidas estocasticas
    signal maxmin_output_sc : STD_LOGIC_VECTOR (0 TO PKG_MLP_N_INPUTS-1);
    signal maxmin_weight_n : pkg_xi_array_t;
    -- LFSR
    signal lfsr_maxmin : STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0);
    signal lfsr_maxmin_signed  : signed (PKG_BIT_PRECISSION-1 DOWNTO 0);

    ----------------------------------------
    -- MLP signals
    ----------------------------------------
    -- LFSR
    signal lfsr_w_mlp : STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0);
    signal lfsr_w_mlp_signed    : signed (PKG_W_WIDTH_BITS-1 DOWNTO 0);
    
    -- SC weights
    signal w_mlp_sc : STD_LOGIC_VECTOR(0 to PKG_MLP_N_WEIGHTS-1);
    signal apc_scale_idx: std_logic_vector(PKG_MLP_APC_SCALE_WIDTH_L1-1 downto 0);

    -- output
    signal y_signed_array : pkg_y_signed_array_t;
    signal o_y_signal     : pkg_y_slv_array_t := (others=>(others=>'0'));	

    -- -- Senales de sincronismo 
    signal rst_apc_hidden : std_logic;
    signal rst_apc_hidden_d0 : std_logic := '0';
    signal rst_apc_hidden_d1 : std_logic := '0';

    signal rst_apc_last_layer : std_logic;
    signal get_output_now : std_logic;

    -- signal w_maxmin_array :  pkg_w_maxmin_array_t;

    component lfsr_nbits IS
    generic (
        GNRC_N_BITS : integer:=2
    );
    PORT(
        -- Inputs
        clk, rst : IN STD_LOGIC; 
        portIn_en : IN STD_LOGIC; 
        portIn_seed: in STD_LOGIC_VECTOR(GNRC_N_BITS-1 downto 0);
        --Outputs
        portOut_lfsr : OUT STD_LOGIC_VECTOR(GNRC_N_BITS-1 downto 0)
    );
    END component;

    component maxmin_layer is
        generic (				
            GNRC_MAXMIN_N_NEURONS           : integer   := 4;  
            GNRC_ADDITION_SCALE_DOWN        : INTEGER   := 2;
            GNRC_BIT_PRECISSION             : INTEGER   := 8
        );    
        Port (
            clk, rst:           IN  STD_LOGIC;
            rst_apc_hidden : in std_logic;
            lfsr_maxmin_signed: IN  signed                  (GNRC_BIT_PRECISSION-1 DOWNTO 0);
            i_x :               in  pkg_xi_array_t;
            i_w :               in  pkg_w_array_t;
            o_y :               out std_logic_vector        (0 to GNRC_MAXMIN_N_NEURONS-1)		
        );
    end component;

    component neurona_sc IS
    generic (				
        NEURON_N_INPUTS: integer:=29;		
        NEURON_F_TRANSFER : integer := 1;
          NEURON_INTG_PRD_CLK : integer := 256-1;
        NEURON_APC_MAX : integer := 7907;
        NEURON_APC_SCALE_WIDTH : integer := 8
        -- NEURON_APC_SCALE : integer := 1
    );
    PORT
    (
        -- Inputs
        clock : IN STD_LOGIC; 
        reset : IN STD_LOGIC;
        
        portIn_apc_scale_idx : IN std_logic_vector(NEURON_APC_SCALE_WIDTH-1 downto 0);
        portIn_bs_tick : 		IN STD_LOGIC; 			
        portIn_lfsr : 		IN STD_LOGIC_VECTOR (log2ceil(NEURON_INTG_PRD_CLK)-1 DOWNTO 0);
        portIn_x_sc : 		IN STD_LOGIC_VECTOR (0 to NEURON_N_INPUTS-1);
        portIn_w_sc : 		IN STD_LOGIC_VECTOR (0 to NEURON_N_INPUTS); -- un peso m�s que la portIn_x_sc. El LSB es el bias.
        portIn_0_sc : 		in std_logic;
        
        
        --Outputs
        portOut_y_sc : 		OUT STD_LOGIC;	
        portOut_apc_full : 		out signed(log2ceil(NEURON_N_INPUTS+2) + log2ceil(NEURON_INTG_PRD_CLK) downto 0);
        portOut_apc_scaled :		out signed(log2ceil(NEURON_INTG_PRD_CLK)-1 DOWNTO 0)  
    );
    
    END component;

    
    component sinc_net is
        Port (
            clock, reset, enable: IN STD_LOGIC;
            out_bs_tick : out std_logic;
            out_rst_apc_hidden : out std_logic;
            out_rst_apc_lastlayer : out std_logic;
            out_done : out std_logic
        );
    end component;

begin

--------------------------------------------------------------------------------
-- MAXMIN LAYER 
--------------------------------------------------------------------------------
    -- LFSR para neuronas MaxMin
    inst_lfsrIn : lfsr_nbits
    generic map(GNRC_N_BITS => PKG_BIT_PRECISSION)
    port map (clock, reset, enable, lfsr1_seed, lfsr_maxmin);

    lfsr_maxmin_signed <= signed (lfsr_maxmin(PKG_BIT_PRECISSION-1 downto 0));


    inst_maxmin_layer : maxmin_layer
    generic map(GNRC_MAXMIN_N_NEURONS => PKG_MAXMIN_N_NEURONS,
                GNRC_ADDITION_SCALE_DOWN => PKG_MAXMIN_ADDITION_SCALE_DOWN,
                GNRC_BIT_PRECISSION => PKG_BIT_PRECISSION
                )        
    port map (
        clock, 
        reset, 
        rst_apc_hidden,
        lfsr_maxmin_signed, 
        i_x, 
        i_w, 
        maxmin_output_sc
    );
        


--------------------------------------------------------------------------------
-- MLP LAYER 
--------------------------------------------------------------------------------
    -- LFSR de pesos
    inst_lfsrW : lfsr_nbits
    generic map(GNRC_N_BITS => PKG_BIT_PRECISSION)
    port map (clock,reset, enable, lfsr2_seed, lfsr_w_mlp);
    
    -- pasamos a entero con signo	
    lfsr_w_mlp_signed<= signed(lfsr_w_mlp(PKG_W_WIDTH_BITS-1 downto 0));

    --------------------------------------------------------------------------------
    -- Linear Layer Weights converison B2S
    --------------------------------------------------------------------------------
    -- Weights Linear Layer
    b2p_w_inst: FOR index IN 0 TO PKG_MLP_N_WEIGHTS-1 GENERATE 
        w_mlp_sc(index)<='1' when i_w(index + PKG_MAXMIN_N_WEIGHTS) >= lfsr_w_mlp_signed else '0';
    END GENERATE;

    --------------------------------------------------------------------------------
    -- Conexion de neuronas
    --------------------------------------------------------------------------------
    Gen_mlp_layer : for I in 0 to PKG_MLP_N_OUTPUTS-1 generate
        mlp_neuron_inst : neurona_sc
        generic map(PKG_MLP_N_INPUTS, 
                    PKG_MLP_FA_OFF, 
                    PCKG_TIME_INTEGRATION_APC_HIDDEN, 
                    PKG_MLP_APC_OFFSET_L1, 
                    PKG_MLP_APC_SCALE_WIDTH_L1)
        port map (
            clock, 
            reset,
            apc_scale_idx, 
            rst_apc_hidden, 
            lfsr_maxmin, -- lfsr is not used
            maxmin_output_sc, -- input
            w_mlp_sc(I*PKG_MLP_N_WEIGHTS_P_NEURON to (I+1)*PKG_MLP_N_WEIGHTS_P_NEURON-1), 
            '0', -- relu0 not used
            open, 
            y_signed_array(I), 
            open
        );
        --mlp_neuron_w_sc <= w_mlp_sc(I*PKG_MLP_N_WEIGHTS_P_NEURON to (I+1)*PKG_MLP_N_WEIGHTS_P_NEURON);
        apc_scale_idx <= (others=>'0');
    end generate Gen_mlp_layer;
--------------------------------------------------------------------------------
-- Sinc module
--------------------------------------------------------------------------------
    -- Sinc Net
    sinc_inst : sinc_net
    port map (
            clock, 
            reset, 
            enable, 
            o_next, 
            rst_apc_hidden_d0, 
            open, 
            open);

--  -------------------------------------------
    -- Si optimizacion en speed On:
    -- agrega DFF en path
    -------------------------------------------
    speed_opt_on_gen : if PKG_SPEED_OPT=True generate  
        process(clock)
        begin
            if rising_edge(clock) then
                rst_apc_hidden_d1 <= rst_apc_hidden_d0;
            end if;
        end process;
        rst_apc_hidden <= rst_apc_hidden_d1;
    end generate speed_opt_on_gen;
    ---------------------------------------
    -- Si optimizacion en speed Off:
    -- no agrega DFF en path
    -------------------------------------------
    speed_opt_off_gen : if PKG_SPEED_OPT=False generate
        rst_apc_hidden <= rst_apc_hidden_d0;
    end generate speed_opt_off_gen;
--------------------------------------------------------------------------------
-- Output layer
--------------------------------------------------------------------------------
    -- output Register
    process(clock)
    begin
         if rising_edge(clock) then 
              o_done <= rst_apc_hidden; -- extra delay
              if (rst_apc_hidden='1') then 	
                   for i in 0 to PKG_MLP_N_OUTPUTS-1 loop 
                          o_y_signal(i) <= std_logic_vector(y_signed_array(i));
                   end loop;
              end if;
         end if;
    end process; 
    o_y <= o_y_signal;

END Behavioral;
